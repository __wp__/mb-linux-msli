#! /bin/bash

ln -sf "$PWD"/vendors/MeyerSound/dmitri-io/extras/libtc.mk user/libtc/Makefile

ln -sf "$PWD"/vendors/MeyerSound/dmitri-io/extras/microsupport.mk lib/microsupport/Makefile

ln -sf "$PWD"/vendors/MeyerSound/dmitri-io/extras/avdecc.mk lib/avdecc/Makefile

ln -sf "$PWD"/vendors/MeyerSound/dmitri-io/extras/io-osc.mk user/io-osc/Makefile

ln -sf "$PWD"/vendors/MeyerSound/dmitri-io/extras/avbweb.mk user/avbweb/Makefile


