/* vi: set sw=4 ts=4; */

#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <poll.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>

#ifndef BUF_SIZE
#define BUF_SIZE ((64/4)*4096)
#endif

/* #define fprintf(...) */

ssize_t safe_read(int fd, void *buf, size_t count)
{
	ssize_t n;

	do {
		n = read(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}

ssize_t safe_write(int fd, const void *buf, size_t count)
{
	ssize_t n;

	do {
		n = write(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}

/*
 * Write all of the supplied buffer out to a file.
 * This does multiple writes as necessary.
 * Returns the amount written, or -1 on an error.
 */
ssize_t full_write(int fd, const void *buf, size_t len)
{
	ssize_t cc;
	ssize_t total;

	total = 0;

	while (len) {
		cc = safe_write(fd, buf, len);

		if (cc < 0) {
			if (total) {
				/* we already wrote some! */
				/* user can do another write to know the error code */
				return total;
			}
			return cc;	/* write() returns -1 on failure. */
		}

		total += cc;
		buf = ((const char *)buf) + cc;
		len -= cc;
	}

	return total;
}


/*
 * Read all of the supplied buffer from a file.
 * This does multiple reads as necessary.
 * Returns the amount read, or -1 on an error.
 * A short read is returned on an end of file.
 */
ssize_t full_read(int fd, void *buf, size_t len)
{
	ssize_t cc;
	ssize_t total;

	total = 0;

	while (len) {
		cc = safe_read(fd, buf, len);

		if (cc < 0) {
			if (total) {
				/* we already have some! */
				/* user can do another read to know the error code */
				return total;
			}
			return cc; /* read() returns -1 on failure. */
		}
		if (cc == 0)
			break;
		buf = ((char *)buf) + cc;
		total += cc;
		len -= cc;
	}

	return total;
}

void show_usage(const char *name, const char *help)
{
	const char *format_string;

	format_string = "\nUsage: %s (%s %s) %s\n\n";
	fprintf(stderr, format_string, name, __DATE__, __TIME__, help);
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	const int timeout = 1000 * 60 * 3;
	ssize_t n;
	char *buf = NULL; /*  [BUF_SIZE]; */
	struct sockaddr src_addr;
	socklen_t addrlen;
	struct pollfd fds[2] = {
		{
			.fd = STDIN_FILENO,
			.events = POLLIN,
		},
		{
			.events = POLLIN,
		},
	};

	if (argc != 2)
		show_usage(argv[0], " provide device path");

	do {
		fds[1].fd = open(argv[1], O_RDWR | O_NONBLOCK);
	} while (fds[1].fd == -1 && errno == EINTR);

	if (fds[1].fd == -1) {
		fprintf(stderr, "%s: couldn't open %s\n", argv[0], argv[1]);
		exit(EXIT_FAILURE);
	}

	buf = malloc(BUF_SIZE);
	if (!buf) {
		fprintf(stderr, "%s: malloc() failed\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	n = recvfrom(STDIN_FILENO, buf, BUF_SIZE, 0, &src_addr,
			&addrlen);
	if (n < 0)
		goto exit;

	while (1) {
		int ready;

		ready = poll(fds, 2, timeout);
		if (ready < 0) {
			fprintf(stderr, "poll() failed\n", argv[0]);
			break;
		}
		if (fds[1].revents & POLLIN) {
			n = full_read(fds[1].fd, buf, BUF_SIZE);
			if (n < 0) {
				fprintf(stderr, "%s: read() on RFM failed\n",
					argv[0]);
				continue;
			}
			if (n > 0) {
				if (sendto(STDOUT_FILENO, buf, n, 0, &src_addr,
						addrlen) < 0)
					fprintf(stderr, "%s: write() on stdout failed\n",
							argv[0]);
			}
		}
		if (fds[0].revents & POLLIN) {
			n = recvfrom(STDIN_FILENO, buf, BUF_SIZE, 0, &src_addr,
					&addrlen);
			if (n < 0) {
				fprintf(stderr, "%s: full_read() failed\n", argv[0]);
				continue;
			}
			if (!n)
				continue;
			if (full_write(fds[1].fd, buf, n) < 0) {
				fprintf(stderr, "%s: full_write() on RFM failed\n",
						argv[0]);
				continue;
			}
		}
		if (ready == 0)
			break; /* timeout */

	}
exit:
	free(buf);
	while (close(fds[1].fd) == -1 && errno == EINTR)
		continue;

	exit(EXIT_SUCCESS);
}

