#ifndef __SOCKET_H__
#define __SOCKET_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define	SOCKET_DEVICE		"eth0"
#define SOCKET_PORT			4000
#define SOCKET_BACKLOG		3
#define	SOCKET_BUFFER_SIZE	4096

int socket_init_server(void);
int socket_close(int fd);

#endif /* __SOCKET_H__ */
