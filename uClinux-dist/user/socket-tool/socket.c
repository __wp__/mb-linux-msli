#include "socket.h"

// initialize server socket.
int socket_init_server (void) {
	struct sockaddr_in local;
	struct ifreq ifr;
	int fd_server, on = 1;

	// create server socket
	fd_server = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd_server == -1) {
		return (-1);
	}

	// allow to use the same socket without a timeout
	if (setsockopt(fd_server, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
		return (-1);
	}

	// allow any ip-address to connect
	local.sin_addr.s_addr = INADDR_ANY;
	// socket port
	local.sin_port = htons(SOCKET_PORT);
	local.sin_family = AF_INET;

	// bind socket to the ethernet interface
	if (bind(fd_server, (struct sockaddr*)&local, sizeof(local)) == -1) {
		return (-1);
	}
/*
	// set server socket into listening mode
	if (listen(fd_server, SOCKET_BACKLOG) == -1) {
		return (-1);
	}
*/

  // get local ip-address
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, SOCKET_DEVICE, IFNAMSIZ-1);
	ioctl(fd_server, SIOCGIFADDR, &ifr);
	printf("status: local ip <%s>\n", 
    inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));

	return (fd_server);
}


// close socket
int socket_close (int fd) {
	return (close(fd));
}
