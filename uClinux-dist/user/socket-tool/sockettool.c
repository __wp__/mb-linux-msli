#include "socket.h"
#include <signal.h>

void sigquit(int signum);

static int fd_server, fd_client;

int main (void)
{
	char buffer[SOCKET_BUFFER_SIZE]; // [128*1024];
  struct sockaddr_in cli_addr;
	int bytes, cli_len;

  (void) signal(SIGQUIT, sigquit);

	fd_server = socket_init_server();
	if (fd_server == -1) {
		printf("error: couldn't open server socket\n");
		return EXIT_FAILURE;
	} else {
		printf("status: opened server socket <%d>\n", fd_server);
	}

	while (1) {
    {
      cli_len = sizeof(cli_addr);
//			printf("status: opened client socket <%d>\n", fd_client);
			while ((bytes = recvfrom(fd_server, buffer, sizeof(buffer), 0, 
        (struct sockaddr*)&cli_addr, &cli_len)) > 0) {
				buffer[bytes] = '\0';
				printf("status: message received <%s>\n", buffer);
        sendto(fd_server, buffer, bytes, 0, 
          (struct sockaddr*)&cli_addr, cli_len);
				printf("status: message sent <%s>\n", buffer);
			}
			socket_close(fd_client);
		}
	}

	socket_close(fd_server);

	return EXIT_SUCCESS;
}

void sigquit(int signum) {
    printf("Exit program. %s catched.", signum);
    socket_close(fd_client);
    socket_close(fd_server);
    EXIT_SUCCESS;
}

