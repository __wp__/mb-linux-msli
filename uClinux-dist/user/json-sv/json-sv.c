/* vi: set sw=4 ts=4; */

#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>

#include <errno.h>
#include <string.h>
#include <assert.h>

#include <syslog.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <jansson.h>

#ifndef BUF_SIZE
#define BUF_SIZE 4096
#endif
#ifndef STRING_SIZE
#define STRING_SIZE 256
#endif

#define __stringify_1(x...) #x
#define __stringify(x...) __stringify_1(x)

#ifndef DEBUG
#define fprintf(...)
#endif

ssize_t safe_read(int fd, void *buf, size_t count)
{
	ssize_t n;

	do {
		n = read(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}

ssize_t safe_write(int fd, const void *buf, size_t count)
{
	ssize_t n;

	do {
		n = write(fd, buf, count);
	} while (n < 0 && errno == EINTR);

	return n;
}

/*
 * Write all of the supplied buffer out to a file.
 * This does multiple writes as necessary.
 * Returns the amount written, or -1 on an error.
 */
ssize_t full_write(int fd, const void *buf, size_t len)
{
	ssize_t cc;
	ssize_t total;

	total = 0;

	while (len) {
		cc = safe_write(fd, buf, len);

		if (cc < 0) {
			if (total) {
				/* we already wrote some! */
				/* user can do another write to know the error code */
				return total;
			}
			return cc;	/* write() returns -1 on failure. */
		}

		total += cc;
		buf = ((const char *)buf) + cc;
		len -= cc;
	}

	return total;
}


/*
 * Read all of the supplied buffer from a file.
 * This does multiple reads as necessary.
 * Returns the amount read, or -1 on an error.
 * A short read is returned on an end of file.
 */
ssize_t full_read(int fd, void *buf, size_t len)
{
	ssize_t cc;
	ssize_t total;

	total = 0;

	while (len) {
		cc = safe_read(fd, buf, len);

		if (cc < 0) {
			if (total) {
				/* we already have some! */
				/* user can do another read to know the error code */
				return total;
			}
			return cc; /* read() returns -1 on failure. */
		}
		if (cc == 0)
			break;
		buf = ((char *)buf) + cc;
		total += cc;
		len -= cc;
	}

	return total;
}


/* Find out if the last character of a string matches the one given.
 * Don't underrun the buffer if the string length is 0.
 */
char *last_char_is(const char *s, int c)
{
	if (s && *s) {
		size_t sz = strlen(s) - 1;
		s += sz;
		if ( (unsigned char)*s == c)
			return (char*)s;
	}
	return NULL;
}


char *xasprintf(const char *format, ...)
{
	va_list p;
	int r;
	char *string_ptr;

	va_start(p, format);
	r = vasprintf(&string_ptr, format, p);
	va_end(p);

	return string_ptr;
}


/* concatenate path and file name to new allocation buffer,
 * not adding '/' if path name already has '/'
 */
char *concat_path_file(const char *path, const char *filename)
{
	char *lc;

	if (!path)
		path = "";
	lc = last_char_is(path, '/');
	while (*filename == '/')
		filename++;
	return xasprintf("%s%s%s", path, (lc==NULL ? "/" : ""), filename);
}


const char *get_full_path(const char *path, const char *group, const char *file)
{
	const char *path_group, *path_group_file;

	if (!path && !group && !file)
		return NULL;
	if (!(path_group = concat_path_file(path, group)))
		return NULL;
	path_group_file = concat_path_file(path_group, file);
	free((void *)path_group);
	return path_group_file;
}


// Read one line a-la fgets. Works only on seekable streams
char *reads(int fd, char *buffer, size_t size)
{
	char *p;

	if (size < 2)
		return NULL;
	size = full_read(fd, buffer, size-1);
	if ((ssize_t)size <= 0)
		return NULL;

	buffer[size] = '\0';
	p = strchr(buffer, '\n');
	if (p) {
		off_t offset;
		*p++ = '\0';
		// avoid incorrect (unsigned) widening
		offset = (off_t)(p-buffer) - (off_t)size;
		// set fd position right after '\n'
		if (offset && lseek(fd, offset, SEEK_CUR) == (off_t)-1)
			return NULL;
	}
	return buffer;
}


char
*malloc_fgets_str(FILE *file, const char terminating_char)
{
	char *linebuf = NULL;
	int linebufsz = 0;
	int idx = 0;
	int ch;

	while (1) {
		ch = fgetc(file);
		if (ch == EOF) {
			if (idx == 0)
				return linebuf; /* NULL */
			break;
		}

		if (idx >= linebufsz) {
			linebufsz += 200;
			linebuf = realloc(linebuf, linebufsz);
			if (!linebuf)
				return NULL;
		}

		linebuf[idx] = ch;
		idx++;

		/* Check for terminating char */
		if (idx >= 1 && (linebuf[idx - 1] == terminating_char))
			break;
	}
	/* Grow/shrink *first*, then store NUL */
	linebuf = realloc(linebuf, idx + 1);
	if (!linebuf)
		return NULL;
	linebuf[idx] = '\0';
	return linebuf;
}



const char
*__get_param(const char *path, const char *group, const char *file)
{
	int fd;
	const char *full_path;
	char *p;
	static char buf[STRING_SIZE];

	full_path = get_full_path(path, group, file);
	if (!full_path)
		return NULL;

	do {
		fd = open(full_path, O_RDONLY);
	} while (fd == -1 && errno == EINTR);
	free((void *)full_path);
	if (fd == -1)
		return NULL;

	p = reads(fd, buf, STRING_SIZE);
	while (close(fd) == -1 && errno == EINTR)
		continue;

	return p;
}


ssize_t
set_param(const char *path, const char *group, const char *file, const char *val)
{
	int fd;
	ssize_t n;
	const char *full_path;

	full_path = get_full_path(path, group, file);
	if (!full_path)
		return -1;

	do {
		fd = open(full_path, O_WRONLY);
	} while (fd == -1 && errno == EINTR);
	free((void *)full_path);
	if (fd == -1) {
		fprintf(stderr, "%s: couldn't open file '%s'\n", __func__, file);
		return -1;
	}
	n = safe_write(fd, val, strlen(val));
	close(fd);

	return n;
}


int
get_param_int(const char *path, const char *group, const char *file, const int in_hex)
{
	const char *p  = __get_param(path, group, file);
	if (!p)
		return -1;

	if (in_hex)
		return strtol(p, NULL, 16);
	else
		return atoi(p);
}


int
get_param_boolean(const char *path, const char *group, const char *file)
{
	const char *p = __get_param(path, group, file);
	if (!p)
		return -1;
	return (!strcmp(p, "Y") ? 1 : 0);
}

const char
*get_param_string(const char *path, const char *group, const char *file)
{
	const char * p = __get_param(path, group, file);
	return p;
}

static int datareq_single = 0;

const char
*get_param_datareq_mode(const char *path, const char *group)
{
	static const char * const lut[] = {"continuous", "single", "off"};
	const char *p;

	p = __get_param(path, group, "datareq_continuous");
	if (!p)
		return NULL;
	if (!strcmp(p, "Y"))
		return lut[0];
	if (datareq_single) {
		datareq_single = 0;
		return lut[1];
	}

	return lut[2];
}


json_t
*encode_config_group(const char *path)
{
	const char const group[] = "config";
	const int stop_bits = 7;
	json_t *obj;
	int tmp;
	const char *p;

	obj = json_object();
	tmp = get_param_int(path, group, "datareq_period", 0);
	if (tmp != -1)
		json_object_set_new(obj,
				"datareq_period",
				json_integer(tmp));
	p = get_param_datareq_mode(path, group);
	if (p)
		json_object_set_new(obj,
				"datareq_mode",
				json_string(p));
	p = get_param_string(path, group, "power_main_red");
	if (p)
		json_object_set_new(obj,
				"power_main_red",
				json_string(p));
	p = get_param_string(path, group, "comm_main_red");
	if (p)
		json_object_set_new(obj,
				"comm_main_red",
				json_string(p));
	tmp = get_param_boolean(path, group, "loopback");
	if (tmp != -1)
		json_object_set_new(obj,
				"loopback",
				tmp ? json_true() : json_false());
	tmp = get_param_boolean(path, group, "pps_en");
	if (tmp != -1)
		json_object_set_new(obj,
				"pps_en",
				tmp ? json_true() : json_false());
	json_object_set_new(obj, "stop_bits", json_integer(stop_bits));

	return obj;
}


void show_usage(const char *name, const char *help)
{
	const char *format_string;

	format_string = "\nUsage: %s (%s %s) %s\n\n";
	fprintf(stderr, format_string, name, __DATE__, __TIME__, help);
	exit(EXIT_FAILURE);
}

#if 0
json_t
*do_diagnostic_comm(const char *path, json_t *obj)
{
	json_t *a_obj, *val_obj;
	int n, tmp;
	const char *dump;
	time_t timestamp;

	a_obj = json_object_get(obj, "tx_test");
	if (!a_obj || !json_is_array(a_obj))
		return NULL;
	n = 0;
	while (1) {
		val_obj = json_array_get(a_obj, n++);
		if (!val_obj)
			break;
		if (!json_is_integer(val_obj))
			continue;
		dump = json_dumps(val_obj, JSON_ENCODE_ANY);
		if (!dump)
			continue;
		set_param(path,
				"diagnostic",
				"comm",
				dump);
		free((void *)dump);
	}
	/* readback and compose JSON object */
	obj = json_copy(obj);
	if (!obj)
		return NULL;
	a_obj = json_object_get(obj, "tx_test");
	if (!a_obj || !json_is_array(a_obj)) {
		json_decref(obj);
		return NULL;
	}
	n = 0;
	timestamp = time(NULL) + 2;
	while (1) {
		tmp = get_param_int(path,
				"diagnostic",
				"comm", 1);
		if (tmp < 0)
			if (timestamp < time(NULL))
				break;
			else
				continue;
		else
			timestamp = 0;
		if (json_array_set_new(a_obj, n, json_integer(tmp))) {
			if (n > json_array_size(a_obj)) {
				if (!json_array_append_new(a_obj,
						json_integer(tmp)))
					n++;
			}
			continue;
		} else
			n++;
	}
	/* if there any remaining elements, delete them */
	while (1) {
		if (json_array_remove(a_obj, n))
			break;
	}
	return obj;
}
#endif

json_t
*apply_config(const char *path, json_t *config_obj)
{
	const char *key;
	json_t *val;
	void *iter = json_object_iter(config_obj);

	if (!iter)
		goto out;
	while (iter) {
		key = json_object_iter_key(iter);
		val = json_object_iter_value(iter);
		iter = json_object_iter_next(config_obj, iter);

		switch (json_typeof(val)) {
		case JSON_NULL:
		break;
		case JSON_TRUE:
			if (set_param(path,
					"config",
					key,
					"y") == -1)
				goto err_out;
		break;
		case JSON_FALSE:
			if (set_param(path,
					"config",
					key,
					"n") == -1)
				goto err_out;
		break;
		case JSON_REAL:
		break;
		case JSON_INTEGER:
		if (!strcmp(key, "datareq_period")) {
			const char *dump = json_dumps(val, JSON_ENCODE_ANY);
			if (!dump)
				break;
			if (set_param(path,
					"config",
					"datareq_period",
					dump) == -1) {
				free((void *)dump);
				goto err_out;
			}
			free((void *)dump);
		}
		break;
		case JSON_OBJECT:
		case JSON_ARRAY:
		break;
		case JSON_STRING:
		if (!strcmp(key, "datareq_mode")) {
			if (!strcmp(json_string_value(val), "off")) {
				if (set_param(path, "config",
						"datareq_continuous",
						"n") == -1)
					goto err_out;
			}
			if (!strcmp(json_string_value(val), "single")) {
				if (set_param(path,
						"config",
						"datareq_continuous",
						"n") == -1)
					goto err_out;
				if (set_param(path,
						"config",
						"datareq_on",
						"y") == -1)
					goto err_out;
				else
					datareq_single = 1;
			}
			if (!strcmp(json_string_value(val), "continuous")) {
				if (set_param(path,
						"config",
						"datareq_continuous",
						"y") == -1)
					goto err_out;
				if (set_param(path,
						"config",
						"datareq_on",
						"y") == -1)
					goto err_out;
			}
			break;
		}
		if (set_param(path, "config", key,
				json_string_value(val)) == -1)
			goto err_out;
		break;
		}
	}
out:
	return encode_config_group(path);
err_out:
	return NULL;
}


int
main(int argc, char *argv[])
{
	json_t *parsed_obj, *config_obj;
	char *p;
	int len;

	if (argc != 2)
		show_usage(argv[0], "provide sysfs path");

	while (1) {
		alarm(60 * 10);

		p = malloc_fgets_str(stdin, '\n');
		if (!p)
			exit(EXIT_FAILURE); /* out of memory */
		if (strlen(p) == 0)
			break; /* we're done */
		parsed_obj = json_loads(p, JSON_REJECT_DUPLICATES, NULL);
		free((void *)p);
		if (!parsed_obj) {
			fprintf(stderr, "%s: json_loads() failed\n", argv[0]);
			continue;
		}
		/* comm_obj = do_diagnostic_comm(argv[1], parsed_obj); */
		config_obj = apply_config(argv[1], parsed_obj);
		/*
		if (comm_obj)
			if (json_object_update(comm_obj, config_obj) < 0)
					fprintf(stderr,
			"%s: json_object_update() failed\n", argv[0]);
		*/
		p = json_dumps(config_obj, JSON_COMPACT);
		if (!p) {
			fprintf(stderr, "%s: json_dumps() failed\n", argv[0]);
			continue;
		}
		len = strlen(p);
		p[len] = '\n'; /* replace '\0' with '\n' */
		if (safe_write(STDOUT_FILENO, p, len + 1) < 0)
			fprintf(stderr, "%s: safe_write() failed\n", argv[0]);
		free((void *)p);
		json_decref(parsed_obj);
		/*
		if (comm_obj)
			json_decref(comm_obj); */
		if (config_obj)
			json_decref(config_obj);
	}
	exit(EXIT_SUCCESS);
}

